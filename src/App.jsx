import React, { Fragment } from "react";
import CreateRoutes from "./routes/routes";
import { ToastContainer } from "react-toastify";

import "./App.scss";
import 'antd/dist/antd.css';
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <Fragment>
      <CreateRoutes />
      <ToastContainer theme="dark" />
    </Fragment>
  );
}

export default App;
