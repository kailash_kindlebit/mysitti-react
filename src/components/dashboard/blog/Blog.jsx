import React, { Fragment, useState, useEffect } from "react";
import moment from "moment";
import InfiniteScroll from "react-infinite-scroller";
import { apiData } from "../../../actions";
import DataLoader from "../../../Utils/DataLoader";
import { UsePlaceAuth } from "../../../AppContext/AppContext";

function Blog() {
  const [blogs, setBlogs] = useState([]);
  const [loader, setLoader] = useState(false);
  const itemsPerPage = 6;
  const [hasMoreItems, sethasMoreItems] = useState(true);
  const [records, setrecords] = useState(itemsPerPage);
  const { active, setActive } = UsePlaceAuth()

  const GetBlog = async () => {
    setLoader(true);
    let apiRes = await apiData.blog();
    if (apiRes?.status >= 200 && apiRes?.status <= 399) {
      setBlogs(apiRes?.data?.data);
      setLoader(false);
    }
  };

  const showItems = (blogs) => {
    var items = [];
    for (var i = 0; i < records; i++) {
      items.push(<> {blogs[i]}</>);
    }
    return items;
  };

  const loadMore = () => {
    if (records === blogs.length) {
      sethasMoreItems(false);
    } else {
      setTimeout(() => {
        setrecords(records + itemsPerPage);
      }, 4000);
    }
  };
  useEffect(() => {

      setActive("8")
    GetBlog();
  }, []);

  return (
    <Fragment>
      <div class="banner-section rest-sec" style={{ height: "250px" }}>
      </div>
      <div className="itemBox">
        <div className="rind-the-right-section comedy-sec">
          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div className="white-box-area inner rest-deals">
                  <div className="row">
                    <div className="col-lg-12 col-12">
                      <section className="client-sec comedy">
                        {!loader ? (
                          <div className="testimonial-section products">
                            <div className="head-yelp">
                              <h3>Our Featured Blogs</h3>
                              <p>
                                Find fun places to see and things to do experience
                                the art, museums, music, zoos
                              </p>
                            </div>
                            <div className="row feat-blogs-head">
                              <InfiniteScroll
                                loadMore={loadMore}
                                hasMore={hasMoreItems}
                                loader={<div className="col-lg-12 col-12" style={{ textAlign: "center" }}>loading...</div>}
                              // useWindow={false}
                              >
                                {showItems(
                                  blogs.map((ele) => {
                                    return (
                                      <div
                                        className="col-lg-4 col-12"
                                        key={ele?.ID}
                                      >
                                        <div className="discount-block blog">
                                          <img
                                            src={ele?.image}
                                            alt=""
                                          />
                                          <div className="time-formate">
                                            <p>
                                              <img
                                                src="assets/images/calender.png"
                                                alt=""
                                              />
                                              {moment(ele?.date).format(
                                                "MMMM Do YYYY"
                                              )}
                                            </p>
                                            <p>
                                              <img
                                                src="assets/images/lcation.png"
                                                alt=""
                                              />{" "}
                                              {ele?.title}
                                            </p>
                                          </div>
                                          <h3>{ele?.post_title}</h3>
                                          <p>{ele?.post_content}</p>

                                          {/* <p>{ele?.post_content.split(" ").length>3 ? `${ele?.post_content.split(" ").slice(0,5).join(" ")}...`:ele?.post_content.split(" ").slice(0,5).join(" ")}</p> */}
                                          <a href="#/">
                                            Read More{" "}
                                            <img
                                              src="assets/images/left.png"
                                              alt=""
                                            />
                                          </a>
                                        </div>
                                      </div>
                                    );
                                  }))}
                              </InfiniteScroll>
                            </div>
                          </div>
                        ) : (
                          <DataLoader />
                        )}
                      </section>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </Fragment>
  );
}
export default Blog;
