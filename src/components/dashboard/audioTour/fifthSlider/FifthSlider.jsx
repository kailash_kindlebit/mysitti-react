import React, { Fragment } from "react";
import Slider from "react-slick";

function FifthSlider({ tourData }) {
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay:false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  return (
    <Fragment>
      <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>{tourData?.title}</h3>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio8">
          <Slider {...settings}>
            {tourData?.imagesData &&
              tourData?.imagesData?.map((ele) => {
                return (
                  <div
                    data-aos="zoom-in-right"
                    className="testimonial-block product"
                    key={ele?.main_uiid}
                  >
                    <div className="discount-block audio">
                      <img src={ele?.image_url} alt="" />
                      <img
                        className="play"
                        src="assets/images/play.png"
                        alt=""
                      />
                      <div className="discount-content things">
                        <h3>
                          {ele?.tour_title.split(" ").length > 3
                            ? `${ele?.tour_title
                                .split(" ")
                                .slice(0, 3)
                                .join(" ")}...`
                            : ele?.tour_title.split(" ").slice(0, 3).join(" ")}
                        </h3>
                      </div>
                    </div>
                  </div>
                );
              })}
          </Slider>
        </div>
      </div>
    </Fragment>
  );
}

export default FifthSlider;
