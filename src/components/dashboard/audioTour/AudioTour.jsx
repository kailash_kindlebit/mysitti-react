import React, { Fragment, useState, useEffect } from "react";

import FirstSlider from "./firstSlider/FirstSlider";
import SecondSlider from "./secondSlider/SecondSlider";
import ThirdSlider from "./thirdSlider/ThirdSlider";
import FourthSlider from "./fourthSlider/FourthSlider";
import FifthSlider from "./fifthSlider/FifthSlider";
import { apiData } from "../../../actions";
import DataLoader from "../../../Utils/DataLoader";
import BannerSec from "../restaurent/bannerSec/BannerSec";

function AudioTour() {
  const [tourData, setTourData] = useState("");
  const [loader, setLoader] = useState(false);

  const GetTour = async () => {
    setLoader(true);
    let payload = {
      city: "New York",
    };
    let apiRes = await apiData.audioTour(payload);
    if (apiRes?.status >= 200 && apiRes?.status <= 399) {
      setTourData(apiRes?.data?.data);
      setLoader(false);
    }
  };

  useEffect(() => {
    GetTour();
  }, []);

  return (
    <Fragment>
      <BannerSec/>

      <div className="rind-the-right-section comedy-sec">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div className="white-box-area inner rest-deals">
                <div className="row">
                  <div className="col-lg-12 col-12">
                    {!loader ? (
                      <section className="client-sec comedy">
                        <FirstSlider tourData={tourData[0]} />
                        <SecondSlider tourData={tourData[1]} />
                        <ThirdSlider tourData={tourData[2]} />
                        <FourthSlider tourData={tourData[3]} />
                        <FifthSlider tourData={tourData[4]} />
                      </section>
                    ) : (
                      <DataLoader />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default AudioTour;
