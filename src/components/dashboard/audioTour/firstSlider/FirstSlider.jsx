import React, { Fragment,useEffect,useState} from "react";
import Slider from "react-slick";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { apiData } from '../../../../actions';

function FirstSlider({ tourData }) {
  const [play, setPlay] = useState(false)
  const [main_uiid, setMain_uiid] = useState("624079d9-24b7-4cc7-90de-d6e964fc90aa")
  const [loader, setLoader] = useState(false);
  const [audioData, setAudioData] = useState({});

  const GetAudio = async () => {
      setLoader(true);
      let payload = {
        audioId:main_uiid ,
      };
      let apiRes = await apiData.getShadowAudio(payload);
      if (apiRes?.status >= 200 && apiRes?.status <= 399) {
          setAudioData(apiRes?.data?.data);
          console.log("audioData",apiRes?.data)
          setLoader(false);
      }
     };
    useEffect(() => {
      GetAudio()
    }, [main_uiid])
  var settings = {
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay:false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  return (
    <Fragment>
      <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>{tourData?.title}</h3>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio">
          <Slider {...settings}>
            {tourData?.imagesData &&
              tourData?.imagesData?.map((ele) => {
                return (
                  <div
                    data-aos="zoom-in-right"
                    className="testimonial-block product"
                    key={ele?.main_uiid}
                  >
                    <div className="discount-block audio">
                      <img src={ele?.image_url} alt="" />
                      <img
                        onClick={()=>{setPlay(true);setMain_uiid(ele.main_uiid)}}
                        className="play"
                        src="assets/images/play.png"
                        alt=""
                      />
                      <div className="discount-content things">
                        <h3>
                          {ele?.tour_title.split(" ").length > 3
                            ? `${ele?.tour_title
                                .split(" ")
                                .slice(0, 3)
                                .join(" ")}...`
                            : ele?.tour_title.split(" ").slice(0, 3).join(" ")}
                        </h3>
                      </div>
                    </div>
                  </div>
                );
              })}
          </Slider>
        </div>
      </div>
      <Modal
      show={play}
      onHide={()=>setPlay(false)}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <h3>{audioData.title}</h3>
          <audio src={audioData.audio_url} controls/>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body dangerouslySetInnerHTML={{ __html:audioData.description}}>
      </Modal.Body>

    </Modal>
    </Fragment>
  );
}

export default FirstSlider;
