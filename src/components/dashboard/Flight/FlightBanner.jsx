import React, { useRef } from 'react'
import { Link } from 'react-router-dom'
import { UsePlaceAuth } from '../../../AppContext/AppContext'

const FlightBanner = () => {
	const { place } = UsePlaceAuth()
	const {active,setActive}=UsePlaceAuth()
	const {origin, setOrigin} = UsePlaceAuth(place)
	const {destination, setDestination} = UsePlaceAuth()
    const {originDate, setOriginDate} = UsePlaceAuth();
	const {destinationDate, setDestinationDate} = UsePlaceAuth();
    const SearchRef=useRef();
    const handleClick=()=>{
        SearchRef.current.scrollIntoView({behavior:"smooth"})
    }
  return (
    <div class="banner-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tab-content" id="myTabContent">
                    <div class="checkbox-sec">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1"/>
                          <label class="form-check-label" for="flexRadioDefault1">
                            One Way
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked/>
                          <label class="form-check-label" for="flexRadioDefault2">
                            Round Trip
                          </label>
                        </div>
                        <div class="links-sec">
                            <a href="#">Book International and Domestic Flights</a>
                        </div>
                    </div>
                    <div class="tab-pane fade show active" id="hotel-tab-pane" role="tabpanel" aria-labelledby="hotel-tab" tabindex="0">
                        <div class="select-form">
                            <form>
                            <div class="choose-sec">
															<label for="exampleInputEmail1" class="form-label">From</label>
															<input type="text" class="form-control" id="text" aria-describedby="emailHelp" placeholder={origin} onChange={(e) => {
																setOrigin(e.target.value
																)
															}} />
														</div>
														<div class="choose-sec">
															<label for="exampleInputPassword1" class="form-label">To</label>
															<div>

															</div>
															<input type="text" class="form-control" id="exampleInput" placeholder={destination} onChange={(e) => { setDestination(e.target.value)}} />
														</div>
														<div class="choose-sec">
															<label for="exampleInputEmail1" class="form-label">Departure</label>
															<input type="date" value={originDate} onChange={(e) => { setOriginDate(e.target.value)}}/>

														</div>
														<div class="choose-sec">
															<label for="exampleInputEmail1" class="form-label">Return</label>
															<input type="date" onChange={(e) => { setDestinationDate(e.target.value)}} value={destinationDate}/>
															{/* <select class="form-select" aria-label="Default select example">
										                     <option selected>{moment(new Date(new Date().getTime()+864000000).toISOString().split('T')[0]).                   format(
                                                               " Do MMM YYYY"
                                                             )}</option>
										                     <option value="1">One</option>
										                     <option value="2">Two</option>
										                     <option value="3">Three</option>
										                   </select> */}
														</div>
														<div class="choose-sec">
															<label for="exampleInputEmail1" class="form-label">Traveller & Class</label>
															<select class="form-select" aria-label="Default select example">
																<option selected>01 Travellers</option>
																<option value="1">One</option>
																<option value="2">Two</option>
																<option value="3">Three</option>
															</select>
															<span class="form-label">Economy/Premium Economy</span>
														</div>
                            </form>
                        </div>
                        <div class="specaility-sec">
                        <div class="fare-sec">
                            <p>Select A Fare Type</p>
                            <div class="choose-specaility">
                                <div class="form-check">
                                  <input class="form-check-input" type="checkbox" value="" id="flexCheck1"/>
                                  <label class="form-check-label" for="flexCheck1">
                                    Regular Fares
                                  </label>
                                </div>
                                <div class="form-check">
                                  <input class="form-check-input" type="checkbox" value="" id="flexCheck2"/>
                                  <label class="form-check-label" for="flexCheck2">
                                    Armed Forces Fares
                                  </label>
                                </div>
                                <div class="form-check">
                                  <input class="form-check-input" type="checkbox" value="" id="flexCheck3"/>
                                  <label class="form-check-label" for="flexCheck3">
                                    Student Fares
                                  </label>
                                </div>
                                <div class="form-check">
                                  <input class="form-check-input" type="checkbox" value="" id="flexCheck4"/>
                                  <label class="form-check-label" for="flexCheck4">
                                    Senior Citizen Fares
                                  </label>
                                </div>
                                <div class="form-check">
                                  <input class="form-check-input" type="checkbox" value="" id="flexCheck5"/>
                                  <label class="form-check-label" for="flexCheck5">
                                    Doctors & Nurses Fares
                                  </label>
                                </div>
                            </div>
                        </div>
                        <div class="trending-sec">
                            <div class="trnd-sec">
                                <p>Trending Searches:</p>
                                <div class="choose-specaility">
                                    <div class="form-check">
                                      <label>Memphis</label>
                                      <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11.6375 15.1156C11.4625 14.9406 11.3785 14.7291 11.3855 14.4812C11.3931 14.2333 11.4844 14.0218 11.6594 13.8468L14.1313 11.375H4.375C4.12708 11.375 3.91913 11.291 3.75113 11.123C3.58371 10.9555 3.5 10.7479 3.5 10.5C3.5 10.252 3.58371 10.0441 3.75113 9.87608C3.91913 9.70866 4.12708 9.62495 4.375 9.62495H14.1313L11.6375 7.1312C11.4625 6.9562 11.375 6.74824 11.375 6.50733C11.375 6.26699 11.4625 6.05933 11.6375 5.88433C11.8125 5.70933 12.0205 5.62183 12.2614 5.62183C12.5017 5.62183 12.7094 5.70933 12.8844 5.88433L16.8875 9.88745C16.975 9.97495 17.0371 10.0697 17.0739 10.1718C17.11 10.2739 17.1281 10.3833 17.1281 10.5C17.1281 10.6166 17.11 10.726 17.0739 10.8281C17.0371 10.9302 16.975 11.025 16.8875 11.1125L12.8625 15.1375C12.7021 15.2979 12.5017 15.3781 12.2614 15.3781C12.0205 15.3781 11.8125 15.2906 11.6375 15.1156Z" fill="#FE4D00"/>
                                        </svg>
                                        <label>New York</label>
                                    </div>
                                    <div class="form-check">
                                      <label>Memphis</label>
                                      <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11.6375 15.1156C11.4625 14.9406 11.3785 14.7291 11.3855 14.4812C11.3931 14.2333 11.4844 14.0218 11.6594 13.8468L14.1313 11.375H4.375C4.12708 11.375 3.91913 11.291 3.75113 11.123C3.58371 10.9555 3.5 10.7479 3.5 10.5C3.5 10.252 3.58371 10.0441 3.75113 9.87608C3.91913 9.70866 4.12708 9.62495 4.375 9.62495H14.1313L11.6375 7.1312C11.4625 6.9562 11.375 6.74824 11.375 6.50733C11.375 6.26699 11.4625 6.05933 11.6375 5.88433C11.8125 5.70933 12.0205 5.62183 12.2614 5.62183C12.5017 5.62183 12.7094 5.70933 12.8844 5.88433L16.8875 9.88745C16.975 9.97495 17.0371 10.0697 17.0739 10.1718C17.11 10.2739 17.1281 10.3833 17.1281 10.5C17.1281 10.6166 17.11 10.726 17.0739 10.8281C17.0371 10.9302 16.975 11.025 16.8875 11.1125L12.8625 15.1375C12.7021 15.2979 12.5017 15.3781 12.2614 15.3781C12.0205 15.3781 11.8125 15.2906 11.6375 15.1156Z" fill="#FE4D00"/>
                                        </svg>
                                        <label>New York</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>




  )
}

export default FlightBanner