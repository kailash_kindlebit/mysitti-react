import React, { Fragment, useState } from "react";
import Autocomplete from "react-google-autocomplete";
import { UsePlaceAuth } from "../../../../AppContext/AppContext";

function BannerSec() {
	let [selectedplace, setSelectedPlace] =useState("")
	let {place, setPlace} =UsePlaceAuth()
	console.log("place-banner",place)
  return (
    <Fragment>
		<div class="banner-section rest-sec">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="hotel-tab-pane" role="tabpanel" aria-labelledby="hotel-tab" tabindex="0">
								<div class="select-form restaurent-sec">
									<form>
										<div class="choose-sec">
                                        <Autocomplete
										 className="form-control"
                                         apiKey="AIzaSyAze2Vkj0ZoO03Xlw03L9eimoGM3KCz0cI"
										 placeholder={place}
                                         onPlaceSelected={(city) => {
											setSelectedPlace(city.formatted_address.split(',')[0]);
                                         }}
                                       />
										</div>
									</form>
								</div>
								<div class="action-sec" onClick={()=>{setPlace(selectedplace===""?place:selectedplace)}}>
									<a>Search</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </Fragment>
  );
}
export default BannerSec;
