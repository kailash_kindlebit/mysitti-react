import React, { Fragment } from "react";

import AppPromo from "./appPromo/AppPromo";
import AdverSec from "./adverSec/AdverSec";
import BannerSec from "./bannerSec/BannerSec";
import FoodFilteration from "./foodFilteration/FoodFilteration";
import { useEffect } from "react";
import { UsePlaceAuth } from "../../../AppContext/AppContext";

function Restaurent() {
  const { active, setActive } = UsePlaceAuth()
  useEffect(() => {
    setActive("4")
  }, [])

  return (
    <Fragment>
      <BannerSec/>
      <AdverSec />
      <FoodFilteration />
      <AppPromo />
    </Fragment>
  );
}

export default Restaurent;
