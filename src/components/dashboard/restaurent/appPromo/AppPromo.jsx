import React, { Fragment,useState } from "react";
import { toast } from "react-toastify";
import axios from 'axios';

function AppPromo() {
  const [emaildata, setEmaildata] = useState("");
  const handleSendEmail=()=>{
    if (!emaildata || emaildata.match('^([a-zA-Z0-9]+)@([a-zA-Z0-9]+)\.([a-zA-Z]{2,5})$')==null) {
      toast.error("Please enter valid Email",{
        theme:"colored",
        // className: 'toast-message'
        autoClose: 1500,
      });
    } else {
      toast.success("Thank you.");
      setEmaildata("");
    };
    axios.post('https://kindlebit.com/api/newletters', {
      email: emaildata,
    })
    .then(function (response) {
    })
    .catch(function (error) {
    });
  };
  return (
    <Fragment>
      <div className="app-promo">
        <div className="container">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-6 col-lg-6 mb-4">
              <div className="app-promo-content">
                <h2>Download App Now !</h2>
                <p>
                  Get ... #1 super travel app, join 100 Million+ happy
                  travellers!
                </p>
                <div className="get-sec">
                  <span>
                    <img src="assets/images/call.svg" alt="" />
                  </span>
                  <input
                    type="text"
                    className="form-control"
                    id="text"
                    aria-describedby="emailHelp"
                    placeholder="Enter Your Email Id"
                    value={emaildata}
                    onChange={(e) => setEmaildata(e.target.value)}
                  />
                  <a onClick={()=>handleSendEmail()} href="#/">Get App Link</a>
                </div>
                <p>More ways to get the app</p>
                <div className="app-link-sec">
                  <a href="https://apps.apple.com/"><img src="assets/images/ios.png" alt="" /></a>
                  <a href="https://play.google.com/"><img src="assets/images/play-s.png" alt="" /></a>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-6 col-lg-6 mb-4">
              <div className="app-promo-img">
                <img src="assets/images/app-screens.png" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default AppPromo;
