import React, { Fragment } from "react";
import Slider from "react-slick";

function ThirdSlider({ sliderData }) {
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay:false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  return (
    <Fragment>
      <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>Fine Dining</h3>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio3">
          <Slider {...settings}>
            {sliderData &&
              sliderData?.map((ele) => {
                return (
                  <div
                    data-aos="zoom-in-right"
                    className="testimonial-block product"
                    key={ele?.id}
                  >
                    <a href={ele?.url}>
                      <div className="discount-block">
                        <img src={ele?.image_url} alt="" />
                        <div className="discount-content">
                          <h3>{ele?.name}</h3>
                          <div className="stars">
                            <ul>
                              <li>
                                <i
                                  className="fa fa-star"
                                  aria-hidden="true"
                                ></i>
                              </li>
                              <li>
                                <i
                                  className="fa fa-star"
                                  aria-hidden="true"
                                ></i>
                              </li>
                              <li>
                                <i
                                  className="fa fa-star"
                                  aria-hidden="true"
                                ></i>
                              </li>
                              <li>
                                <i
                                  className="fa fa-star"
                                  aria-hidden="true"
                                ></i>
                              </li>
                            </ul>
                            <p>({ele?.review_count} Reviews)</p>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
                );
              })}
          </Slider>
        </div>
      </div>
    </Fragment>
  );
}

export default ThirdSlider;
