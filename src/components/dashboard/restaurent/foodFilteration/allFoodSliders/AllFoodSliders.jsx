import React, { Fragment, useEffect, useState } from "react";
import FirstSlider from "./firstSlider/FirstSlider";
import SecondSlider from "./secondSlider/SecondSlider";
import ThirdSlider from "./thirdSlider/ThirdSlider";
import FourthSlider from "./fourthSlider/FourthSlider";
import FifthSlider from "./fifthSlider/FifthSlider";
import SixthSlider from "./sixthSlider/SixthSlider";
import DataLoader from "../../../../../Utils/DataLoader";
import { apiData } from "../../../../../actions";
import { UsePlaceAuth } from "../../../../../AppContext/AppContext";

function AllFoodSliders() {
  let {place} =UsePlaceAuth()
  const [loader, setLoader] = useState(false);
  const [sliderData, setSliderData] = useState();
  const AllSliderData = async () => {
    setLoader(true);
    let payload = {
      city: place,
    };
    let apiRes = await apiData.restaurantDealsSearch(payload);
    setSliderData(apiRes);
    setLoader(false);
  };
  useEffect(() => {
    AllSliderData();
  }, [place]);
  return (
    <Fragment>
      <section className="client-sec comedy">
        {!loader ? (
          <>
            <FirstSlider sliderData={sliderData?.deliveryRestaurant} />
            <SecondSlider sliderData={sliderData?.food}/>
            <ThirdSlider sliderData={sliderData?.fineDining} />
            <FourthSlider sliderData={sliderData?.modePricedRestaurants} />
            <FifthSlider sliderData={sliderData?.cheapEats} />
            <SixthSlider sliderData={sliderData?.breakfast} />
          </>
        ) : (
          <DataLoader />
        )}
      </section>
    </Fragment>
  );
}

export default AllFoodSliders;
