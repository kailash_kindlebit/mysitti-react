import React, { Fragment } from "react";
import Slider from "react-slick";
import { UsePlaceAuth } from "../../../../../../AppContext/AppContext";


function SecondSlider({sliderData}) {
	let {place, setPlace} =UsePlaceAuth()
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows:true,
    autoplay:false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  return (
    <Fragment>
      <div className="testimonial-section products">
        <div className="head-yelp">
          <h3> {place} by Food </h3>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio2">
          <Slider {...settings}>
          {sliderData &&
              sliderData?.map((ele) => {
                return (
                  <a href={ele?.url}>
                  <div data-aos="zoom-in-right" className="testimonial-block product">
                  <div className="discount-block">
                    <img src={ele?.image_url} alt="" />
                    <div className="discount-content">
                      <h3>{ele?.name}</h3>
                    </div>
                  </div>
                </div>
                </a>
                )
                })}


            <div data-aos="zoom-in-left" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/rest6.png" alt="" />
                <div className="discount-content">
                  <h3>Italian</h3>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-right" className="testimonial-block product">
              <div className="discount-block city">
                <img src="assets/images/rest7.png" alt="" />
                <div className="discount-content">
                  <h3>Pizza</h3>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-left" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/rest8.png" alt="" />
                <div className="discount-content">
                  <h3>Chinese</h3>
                </div>
              </div>
            </div>
          </Slider>
        </div>
      </div>
    </Fragment>
  );
}

export default SecondSlider;
