import React, { Fragment, useState } from "react";

import AllFoodSliders from "./allFoodSliders/AllFoodSliders";
import JsonData from "../../restaurent/jsonFilterData";
import DynamicDataFilteration from "../../restaurent/foodFilteration/dynaminDataFilteration";

import { apiData } from "../../../../actions";
import { UsePlaceAuth } from "../../../../AppContext/AppContext";

function FoodFilteration() {
  const [selectedCheckbox, setSelectedCheckbox] = useState("");
  const [sliderData, setSliderData] = useState();
  const [testing, setTesting] = useState([]);
  const [scrollComponent, setScrollComponent] = useState(false);
  const DataJson = JsonData ?? [];
  const{place,setPlace}=UsePlaceAuth()
console.log("place-restaurant",place)
  const FilterData = async () => {
    let payload = {
      city: place,
      keyword: selectedCheckbox,
    };
    let apiRes = await apiData.restaurantDealsFilterSearch(payload);
    setSliderData(apiRes);
  };

  // test checkbox

  const handleDeliveryType = (e) => {
    const { checked } = e.target;
    setScrollComponent(checked);
    if (checked) {
      const element = document.getElementById("my-element");
      element.scrollIntoView({ behavior: "smooth" });
    }
    const getValue = e.target.value;
    setSelectedCheckbox(getValue);
    setTesting((prevState) => {
      return checked
        ? [prevState, getValue]
        : prevState?.filter((item) => item !== getValue);
    });
  };
  return (
    <Fragment>
      <div className="itemBox">
              <div className="rind-the-right-section comedy-sec">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3">
              <div className="sidebar-listing">
                <div className="specialities-checkbox">
                  <div className="searcher-sec">
                    <label className="custom-control-label">
                      Search By Name
                    </label>
                    <div className="form-group">
                      <input
                        type="Name"
                        className="form-control"
                        id="exampleFormControlInput1"
                        placeholder={place}
                      />
                    </div>
                  </div>
                </div>
                <div className="specialities-checkbox">
                  {DataJson &&
                    DataJson?.map((ele, ind) => {
                      return (
                        <Fragment key={ind}>
                          <div className="listing-check">
                            <h4>{ele?.heading}</h4>
                            {ele?.data?.map((data, index) => {
                              return (
                                <div
                                  className="custom-control custom-checkbox"
                                  key={index}
                                >
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    id={data?.label}
                                    value={data?.label}
                                    checked={testing.includes(data?.label)}
                                    onChange={(e) => {
                                      handleDeliveryType(e);
                                      FilterData();
                                    }}
                                  />
                                  <label
                                    className="custom-control-label"
                                    htmlFor={data.label}
                                  >
                                    {data.label}
                                  </label>
                                </div>
                              );
                            })}
                          </div>
                        </Fragment>
                      );
                    })}
                </div>
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-9">
              <div className="white-box-area inner rest-deals">
                <div className="row">
                  <div className="col-md-12 col-12">
                    <div className="heading-content">
                      <div className="content-sec">
                        <p>Showing 3 of 257 places</p>
                        <div className="custom-select-box">
                          <span>Sort by </span>
                          <select
                            className="form-select"
                            aria-label="Default select example"
                          >
                            <option value="">Recommended</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12 col-12">
                    <AllFoodSliders />
                    {/* scrollPage */}
                    <div id="my-element">
                      {selectedCheckbox && (
                       scrollComponent &&  <DynamicDataFilteration
                          {...{ selectedCheckbox, filterData: sliderData }}
                        />
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>

    </Fragment>
  );
}

export default FoodFilteration;
