import React, { Fragment, memo } from "react";

import Slider from "react-slick";

const DynamicDataFilteration = ({
  selectedCheckbox,
  filterData,
}) => {
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
	responsive: [
		{
		  breakpoint: 991,
		  settings: {
			slidesToShow: 3,
		  }
		},
		{
		  breakpoint: 767,
		  settings: {
			slidesToShow: 2,
		  }
		},
		{
		  breakpoint: 499,
		  settings: {
			slidesToShow: 1,
		  }
		}
	  ]
  };
  return (
    <Fragment>
      <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>{`${selectedCheckbox}`}</h3>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio">
          <Slider {...settings}>
            {filterData &&
              filterData?.map((ele) => {
                return (
                  <div
                    data-aos="zoom-in-right"
                    className="testimonial-block product"
                    key={ele?.id}
                  >
                    <div className="discount-block">
                      <a href={ele?.url} target="blank">
                        <img src={ele?.image_url} alt="" />
                      </a>
                      <div className="discount-content">
                        <h3>{ele?.name}</h3>
                        <div className="stars">
                          <ul>
                            <li>
                              <i className="fa fa-star" aria-hidden="true"></i>
                            </li>
                            <li>
                              <i className="fa fa-star" aria-hidden="true"></i>
                            </li>
                            <li>
                              <i className="fa fa-star" aria-hidden="true"></i>
                            </li>
                            <li>
                              <i className="fa fa-star" aria-hidden="true"></i>
                            </li>
                          </ul>
                          <p>({ele?.review_count} Reviews)</p>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
          </Slider>
        </div>
      </div>


      {/*shbtehhmnhjmkjf  */}
      <div class="hotel_listitem">
										<div class="hotel_img">
											<div class="image_htfix">
												<a href="https://www.yelp.com/biz/wah-fung-no-1-new-york-2?adjust_creative=D7NZNM8GzFJhuQiisFFdWg&amp;utm_campaign=yelp_api_v3&amp;utm_medium=api_v3_business_search&amp;utm_source=D7NZNM8GzFJhuQiisFFdWg" target="_blank">
												<img src="https://s3-media4.fl.yelpcdn.com/bphoto/uO14Qw1yVeNln4z8Pr68aQ/o.jpg"/>
												</a>
											</div>
										</div>
										<div class="hotel_details resturant_sprecification">
											<div class="loc_details">
												<div class="hotel_name">
													<h5><a href="https://www.yelp.com/biz/wah-fung-no-1-new-york-2?adjust_creative=D7NZNM8GzFJhuQiisFFdWg&amp;utm_campaign=yelp_api_v3&amp;utm_medium=api_v3_business_search&amp;utm_source=D7NZNM8GzFJhuQiisFFdWg" target="_blank">Wah Fung No 1</a></h5>
												</div>

											</div>

											<div class="restro_btns">
												<div class="restro_deals">
												<a href="https://www.yelp.com/biz/wah-fung-no-1-new-york-2?adjust_creative=D7NZNM8GzFJhuQiisFFdWg&amp;utm_campaign=yelp_api_v3&amp;utm_medium=api_v3_business_search&amp;utm_source=D7NZNM8GzFJhuQiisFFdWg" target="_blank" class="restro_btn">
												VIEW DEAL
												</a>
												</div>
												<div class="restro_deals">
												<a href="https://www.yelp.com/biz/wah-fung-no-1-new-york-2?adjust_creative=D7NZNM8GzFJhuQiisFFdWg&amp;utm_campaign=yelp_api_v3&amp;utm_medium=api_v3_business_search&amp;utm_source=D7NZNM8GzFJhuQiisFFdWg" target="_blank" class="restro_btn">
												ORDER ONLINE
												</a>
												</div>
												<div class="hotel_rating">
													<div class="star_rate">
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="reviews yelpuser-review" style={{cursor:"pointer"}} data-toggle="modal" data-target="#myModal-review" data-id="X8ZS-dgiMIJvhwf9SaDnjw">
														(1975 Ratings)
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="hotel_listitem">
										<div class="hotel_img">
											<div class="image_htfix">
												<a href="https://www.yelp.com/biz/wah-fung-no-1-new-york-2?adjust_creative=D7NZNM8GzFJhuQiisFFdWg&amp;utm_campaign=yelp_api_v3&amp;utm_medium=api_v3_business_search&amp;utm_source=D7NZNM8GzFJhuQiisFFdWg" target="_blank">
												<img src="https://s3-media4.fl.yelpcdn.com/bphoto/uO14Qw1yVeNln4z8Pr68aQ/o.jpg"/>
												</a>
											</div>
										</div>
										<div class="hotel_details resturant_sprecification">
											<div class="loc_details">
												<div class="hotel_name">
													<h5><a href="https://www.yelp.com/biz/wah-fung-no-1-new-york-2?adjust_creative=D7NZNM8GzFJhuQiisFFdWg&amp;utm_campaign=yelp_api_v3&amp;utm_medium=api_v3_business_search&amp;utm_source=D7NZNM8GzFJhuQiisFFdWg" target="_blank">Wah Fung No 1</a></h5>
												</div>

											</div>

											<div class="restro_btns">
												<div class="restro_deals">
												<a href="https://www.yelp.com/biz/wah-fung-no-1-new-york-2?adjust_creative=D7NZNM8GzFJhuQiisFFdWg&amp;utm_campaign=yelp_api_v3&amp;utm_medium=api_v3_business_search&amp;utm_source=D7NZNM8GzFJhuQiisFFdWg" target="_blank" class="restro_btn">
												VIEW DEAL
												</a>
												</div>
												<div class="restro_deals">
												<a href="https://www.yelp.com/biz/wah-fung-no-1-new-york-2?adjust_creative=D7NZNM8GzFJhuQiisFFdWg&amp;utm_campaign=yelp_api_v3&amp;utm_medium=api_v3_business_search&amp;utm_source=D7NZNM8GzFJhuQiisFFdWg" target="_blank" class="restro_btn">
												ORDER ONLINE
												</a>
												</div>
												<div class="hotel_rating">
													<div class="star_rate">
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="reviews yelpuser-review" style={{cursor:"pointer"}} data-toggle="modal" data-target="#myModal-review" data-id="X8ZS-dgiMIJvhwf9SaDnjw">
														(1975 Ratings)
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
    </Fragment>
  );
};

export default memo(DynamicDataFilteration);
