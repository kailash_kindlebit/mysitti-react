import React, { Fragment, useState, useEffect } from "react";

import { Rate } from "antd";
import Slider from "react-slick";

import { apiData } from "../../../../actions";
import { UsePlaceAuth } from "../../../../AppContext/AppContext";
import DataLoader from "../../../../Utils/DataLoader";

function EscapeRoom() {
  let {place, setPlace} =UsePlaceAuth()
  const [loader, setLoader] = useState(false);
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  const [escapeRoomData, setEscapeRoomData] = useState([]);

  const GetEscapeRoom = async () => {
    setLoader(true);
    let payload = {
      city: place,
    };
    let apiRes = await apiData.getEscapeRoom(payload);
    setEscapeRoomData(apiRes?.data?.escape?.businesses);
    setLoader(false);
    console.log("escape room",apiRes?.data)
  };

  useEffect(() => {
    GetEscapeRoom();
  }, [place]);

  return (
    <Fragment>
      {
          !loader?
        <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>{`Escape Room in ${place}`}</h3>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio4">
          <Slider {...settings}>
            {escapeRoomData &&
              escapeRoomData?.map((ele) => {
                return (
                  <div
                    data-aos="zoom-in-right"
                    className="testimonial-block product"
                    key={ele?.id}
                  >
                    <div className="discount-block">
                      <img src={ele?.image_url} alt="" />
                      <div className="discount-content">
                        <h3>{ele?.name}</h3>
                        <div className="stars">
                          <ul>
                            <li>
                              <Rate
                                allowHalf
                                defaultValue={ele?.rating}
                                disabled
                              />
                            </li>
                          </ul>
                          <p>({ele?.review_count})</p>
                          <div className="location-sec">
                            <img src="assets/images/lcation.png" alt="" />
                            <p>{ele.location.city}</p>
                          </div>
                        </div>
                        <a className="card-footer-btn" href={ele.url}>
                          {ele.categories[0].title}

                        </a>
                      </div>
                    </div>
                  </div>
                );
              })}
          </Slider>
        </div>
      </div>
:""
      }

    </Fragment>
  );
}

export default EscapeRoom;
