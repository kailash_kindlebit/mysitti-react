import React, { Fragment } from "react";

import Slider from "react-slick";

function PopularThings() {
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  return (
    <Fragment>
      <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>Popular things to do</h3>
          <p>
            Find fun places to see and things to do experience the art, museums,
            music, zoo
          </p>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio">
          <Slider {...settings}>
            <div data-aos="zoom-in-right" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/th1.png" alt="" />
                <div className="discount-content things">
                  <h3>Museum</h3>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-left" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/th2.png" alt="" />
                <div className="discount-content things">
                  <h3>Sightseeing/tours</h3>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-right" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/th3.png" alt="" />
                <div className="discount-content things">
                  <h3>Day Trip</h3>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-left" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/th4.png" alt="" />
                <div className="discount-content things">
                  <h3>Top Attractions</h3>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-left" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/th1.png" alt="" />
                <div className="discount-content things">
                  <h3>Museum</h3>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-left" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/th4.png" alt="" />
                <div className="discount-content things">
                  <h3>Museum</h3>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
          </Slider>
        </div>
      </div>
    </Fragment>
  );
}

export default PopularThings;
