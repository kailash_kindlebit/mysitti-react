import React, { Fragment, useState, useEffect } from "react";
import Slider from "react-slick";
import { apiData } from "../../../../actions";
import { UsePlaceAuth } from "../../../../AppContext/AppContext";

function AdrenalineRush() {
  let {place} =UsePlaceAuth()
  console.log("place",place);

  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  const [getAdrenalineRush, setAdrenalineRush] = useState([]);
  const [loader, setLoader] = useState(false);

  const GetAdrenalineRush = async () => {
    setLoader(true);
      let payload = {
        city: place,
      };
    let apiRes = await apiData.getAdrenalineRush(payload);
    if (apiRes?.status >= 200 && apiRes?.status <= 399) {
      setAdrenalineRush(apiRes?.data?.adrenalineRush);
      console.log('Avijit',apiRes)
      setLoader(false);
    }
  };

  useEffect(() => {
    GetAdrenalineRush();
  }, [place]);

  return (
    <Fragment>
      {!loader?

        <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>Adrenaline Rush</h3>
          <p>Amazing flight,helicopter tours, and tons of exciting things to do</p>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio4">
        <Slider {...settings}>
{
getAdrenalineRush&&getAdrenalineRush?.map((ele)=>
<div data-aos="zoom-in-right" className="testimonial-block product">
  <div className="discount-block">
  <a  href={ele.link}><img src={ele.image_link} alt="{ele.image_name}"/></a>
    <div className="discount-content">
      <h3> {ele.title} </h3>
      <div className="stars">
        <ul>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
        </ul>
        <p>(339 Reviews)</p>
        <div className="location-sec">
          <img src="assets/images/location.png" alt=""/>
          <p>{`${place}`}</p>
        </div>
      </div>
    </div>
  </div>
</div>
)}
        </Slider>
        </div>
      </div>:""
      }

    </Fragment>
  );
}

export default AdrenalineRush;
