import axios from "axios";
import React, { Fragment, useEffect, useState } from "react";

import Slider from "react-slick";
import { UsePlaceAuth } from "../../../../AppContext/AppContext";

function DigInto() {
  let {place} =UsePlaceAuth()
    var settings = {
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 3,
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2,
            }
          },
          {
            breakpoint: 499,
            settings: {
              slidesToShow: 1,
            }
          }
        ]
      };
      const [digData, setDigData] = useState([]);
      const [loader, setLoader] = useState(false);

      // useEffect(() => {
      //   axios
      //   .get(
      //     `${process.env.REACT_APP_BASE_URL}get-where-to-stay/?city=${place}`,
      //     {
      //       headers: {
      //         "Content-Type": "application/json",
      //       },
      //     }
      //   )
      //   .then(
      //     function (response) {
      //       setDigData(response?.data?.whereToStay);
      //     },
      //     (error) => {
      //       console.log(error);
      //     }
      //   );
      // }, [place]);

      console.log("dig_data_Script",digData[0]?.content?.split('"')[1])
      let script = document.createElement('script');
      let src1 =digData[0]?.content?.split('"')[1];
      script.id="dig"
      script.src=src1
      document.body.appendChild(script);

      useEffect(() => {
        return () => {
          document.querySelectorAll("#stay").src=""
          document.querySelectorAll("#dig").src=""
        }
      }, [])

  return (
    <Fragment>
      {
        !loader?
        <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>Dig Into New York</h3>
          <p>Stories, tips, and guides</p>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio">
          <Slider {...settings}>
            <div data-aos="zoom-in-right" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/th1.png" alt="" />
                <div className="discount-content things">
                  <h3>Museum</h3>
                  <div>
                      <p>Duration: 2 hours</p>
                      <p>5(78965)</p>
                  </div>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-right" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/th1.png" alt="" />
                <div className="discount-content things">
                  <h3>Museum</h3>
                  <div>
                      <p>Duration: 2 hours</p>
                      <p>5(78965)</p>
                  </div>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-right" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/th1.png" alt="" />
                <div className="discount-content things">
                  <h3>Museum</h3>
                  <div>
                      <p>Duration: 2 hours</p>
                      <p>5(78965)</p>
                  </div>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-right" className="testimonial-block product">
              <div className="discount-block">
                <img src="assets/images/th1.png" alt="" />
                <div className="discount-content things">
                  <h3>Museum</h3>
                  <div>
                      <p>Duration: 2 hours</p>
                      <p>5(78965)</p>
                  </div>
                </div>
              </div>
            </div>
          </Slider>
        </div>
      </div>:""
      }

  </Fragment>
  )
}

export default DigInto