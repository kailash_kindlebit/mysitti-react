import React, { Fragment, useState, useEffect } from "react";
import Slider from "react-slick";
import { apiData } from "../../../../actions";
import { UsePlaceAuth } from "../../../../AppContext/AppContext";

function WhereToStay() {
  let {place} =UsePlaceAuth()
  const [digData, setStayData] = useState([]);
  const [loader, setLoader] = useState(false);
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoPlay:false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]

  };
  const GetStay = async () => {
		setLoader(true);
		let payload = {
		  city: place,
		};
		let apiRes = await apiData.getWhereToStay(payload);
		if (apiRes?.status >= 200 && apiRes?.status <= 399) {
		  setStayData(apiRes?.data?.whereToStay);
		  setLoader(false);
		}
	  };

    console.log("state",digData[0]?.content?.split('"')[1])
    let script = document.createElement('script');
    let src1 =digData[0]?.content?.split('"')[1]
    script.id="stay"
    script.src=src1
    document.body.appendChild(script);

    useEffect(() => {
      // GetStay()
    }, [place])
  return (
    <Fragment>
      {
        !loader?
        <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>Where to stay</h3>
          <p>Low hotel rates for luxury, comfort, pet-friendly rooms</p>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio4">
        <Slider {...settings}>

<div data-aos="zoom-in-right" className="testimonial-block product">
  <div className="discount-block">
    <img src="assets/images/th13.png" />
    <div className="discount-content">
      <h3>Thailicious</h3>
      <div className="stars">
        <ul>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
        </ul>
        <p>(339 Reviews)</p>
        <div className="location-sec">
          <img src="assets/images/location.png" />
          <p>New York</p>
        </div>
      </div>
      <a className="card-footer-btn" href="#">ESCAPE GAMES</a>
    </div>
  </div>
</div>
<div data-aos="zoom-in-left" className="testimonial-block product">
  <div className="discount-block">
    <img src="assets/images/th14.png" />
    <div className="discount-content">
      <h3>Thailicious</h3>
      <div className="stars">
        <ul>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
        </ul>
        <p>(339 Reviews)</p>
        <div className="location-sec">
          <img src="assets/images/location.png" />
          <p>New York</p>
        </div>
      </div>
      <a className="card-footer-btn" href="#">ESCAPE GAMES</a>
    </div>
  </div>
</div>
<div data-aos="zoom-in-right" className="testimonial-block product">
  <div className="discount-block">
    <img src="assets/images/th15.png" />
    <div className="discount-content">
      <h3>Thailicious</h3>
      <div className="stars">
        <ul>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
        </ul>
        <p>(339 Reviews)</p>
        <div className="location-sec">
          <img src="assets/images/location.png" />
          <p>New York</p>
        </div>
      </div>
      <a className="card-footer-btn" href="#">ESCAPE GAMES</a>
    </div>
  </div>
</div>
<div data-aos="zoom-in-left" className="testimonial-block product">
  <div className="discount-block">
    <img src="assets/images/rest1.png" />
    <div className="discount-content">
      <h3>Thailicious</h3>
      <div className="stars">
        <ul>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
        </ul>
        <p>(339 Reviews)</p>
        <div className="location-sec">
          <img src="assets/images/location.png" />
          <p>New York</p>
        </div>
      </div>
      <a className="card-footer-btn" href="#">ESCAPE GAMES</a>
    </div>
  </div>
</div>
</Slider>
        </div>
      </div>
      :""
      }

    </Fragment>
  );
}

export default WhereToStay;
