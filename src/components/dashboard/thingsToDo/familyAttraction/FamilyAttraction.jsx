import React, { Fragment, useState, useEffect } from "react";

import Slider from "react-slick";

import { apiData } from "../../../../actions";
import { UsePlaceAuth } from "../../../../AppContext/AppContext";
import DataLoader from "../../../../Utils/DataLoader";

function FamilyAttraction() {
  let {place} =UsePlaceAuth()
  const [loader, setLoader] = useState(false);


  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };

  const [familyAttractionData, setFamilyAttractionData] = useState("");

  const GetFamilyAttraction = async () => {
    setLoader(true);
    let payload = {
      city: place,
    };
    let apiRes = await apiData.getFamilyAttraction(payload);
    setFamilyAttractionData(apiRes?.data?.familytopattr?.businesses);
        setLoader(false);
    console.log("apires GetFamilyAttraction", apiRes?.data?.familytopattr?.businesses);
  };

  useEffect(() => {
    GetFamilyAttraction();
  }, [place]);

  return (
    <Fragment>
      {
!loader?
        <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>{`Family Top Attractions in ${place}`}</h3>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio4">
        <Slider {...settings}>
{
familyAttractionData&&familyAttractionData?.map((ele)=>{
  return (
    <div data-aos="zoom-in-right" className="testimonial-block product">
  <div className="discount-block">
  <a href={ele.url}><img src={ele.image_url}/></a>

    <div className="discount-content">
      <h3>{ele.name}</h3>
      <div className="stars">
        <ul>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
          <li><i className="fa fa-star" aria-hidden="true"></i></li>
        </ul>
        <p>{`${ele.review_count} Reviews`}</p>
        <div className="location-sec">
          <img src="assets/images/location.png" />
          <p>{ele.location.city}</p>
        </div>
      </div>
      <a className="card-footer-btn" href={ele.url}>{ele.categories[0].title}</a>
    </div>
  </div>
</div>
  )
})
}
</Slider>
        </div>
      </div>
      :""
      }

    </Fragment>
  );
}

export default FamilyAttraction;
