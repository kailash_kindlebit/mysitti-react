import React, { Fragment,useState,useEffect } from "react";

import Slider from "react-slick";
import { apiData } from "../../../../actions";
import { UsePlaceAuth } from "../../../../AppContext/AppContext";
import DataLoader from "../../../../Utils/DataLoader";

function TourTravel() {
  let {place} =UsePlaceAuth()

  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  const [tourData, setTourData] = useState([]);
  const [loader, setLoader] = useState(false);

  const GetTourTravel = async () => {
    setLoader(true);
    let payload = {
      city: place,
    };
    let apiRes = await apiData.getToursTravel(payload);
    if (apiRes?.status >= 200 && apiRes?.status <= 399) {
      setTourData(apiRes?.data?.tourstravel);
      setLoader(false);
    }
  };
  useEffect(() => {
    GetTourTravel();
  }, [place]);
  return (
    <Fragment>
      {
!loader?
        <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>Tours & Travel</h3>
          <p>Enjoy the scenic views of National Parks</p>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio">
          <Slider {...settings}>
            {
             tourData&&tourData?.map((ele)=>
             <div data-aos="zoom-in-right" className="testimonial-block product">
             <div className="discount-block">
               <a href={ele.link}><img src={ele.image_link} alt="" /></a>
               <div className="discount-content things">
               <a href={ele.link}><h3>{ele.title}</h3></a>
                 <a href={ele.link}>
                   <i className="fa fa-star-o" aria-hidden="true"></i>
                 </a>
               </div>
             </div>
             </div>
             )
            }

          </Slider>
        </div>
      </div>
      :""
      }

    </Fragment>
  );
}

export default TourTravel;
