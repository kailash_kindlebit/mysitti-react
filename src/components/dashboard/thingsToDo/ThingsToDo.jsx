import React, { Fragment } from "react";

import Winery from "./winery/Winery";
import Comedy from "./comedy/Comedy";
import DigInto from "./digInto/DigInto";
import LocalMusic from "./localMusic/LocalMusic";
import TourTravel from "./tourTravel/TourTravel";
import EscapeRoom from "./escapeRoom/EscapeRoom";
import WhereToStay from "./whereToStay/WhereToStay";
import PopularThings from "./popularThings/PopularThings";
import AdrenalineRush from "./adrenalineRush/AdrenalineRush";
import FamilyAttraction from "./familyAttraction/FamilyAttraction";
import PopularEntertainment from "./popularEntertainment/PopularEntertainment";
import BannerSec from "../restaurent/bannerSec/BannerSec";
import { UsePlaceAuth } from "../../../AppContext/AppContext";
import { useEffect } from "react";

function ThingsToDo() {
  const { active, setActive } = UsePlaceAuth()

  useEffect(() => {
    setActive("5")
  }, [])
  return (
    <Fragment>
      <BannerSec/>
      <div className="itemBox" style={{marginTop:"10px",marginBottom:"10px"}}>
      <div className="rind-the-right-section comedy-sec">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div className="white-box-area inner rest-deals">
                <div className="row">
                  <div className="col-lg-12 col-12">
                    <section className="client-sec comedy">
                      <PopularThings />
                      <LocalMusic />
                      <PopularEntertainment />
                      <EscapeRoom />
                      <FamilyAttraction />
                      <Winery />
                      <Comedy />
                      <AdrenalineRush />
                      <TourTravel />
                      <DigInto />
                      <WhereToStay />
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>

    </Fragment>
  );
}

export default ThingsToDo;
