import React, { Fragment } from "react";
import { Link } from "react-router-dom";

import Slider from "react-slick";

function PopularEntertainment() {
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  return (
    <Fragment>
      <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>Popular Entertainment</h3>
          <p>Tons of exciting things for entertainment</p>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio3">
          <Slider {...settings}>
            <div data-aos="zoom-in-right" className="testimonial-block product">
              <div className="discount-block">
              <Link to="/family"><img src="assets/images/th9.png" alt="" /></Link>
                <div className="discount-content things">
                  <Link to="/family"><h3>Family</h3></Link>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-left" className="testimonial-block product">
              <div className="discount-block">
                <Link to="/performing-arts"><img src="assets/images/th10.png" alt="" /></Link>
                <div className="discount-content things">
                <Link to="/performing-arts"><h3>Performing Arts</h3></Link>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-right" className="testimonial-block product">
              <div className="discount-block">
                <Link to="/brewery"><img src="assets/images/th11.png" alt="" /></Link>
                <div className="discount-content things">
                <Link to="/brewery"><h3>Winery/Brewery</h3></Link>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-left" className="testimonial-block product">
              <div className="discount-block">
                <Link to="/comedy"><img src="assets/images/th12.png" alt="" /></Link>
                <div className="discount-content things">
                <Link to="/comedy"><h3>Comedy</h3></Link>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
          </Slider>
        </div>
      </div>
    </Fragment>
  );
}

export default PopularEntertainment;