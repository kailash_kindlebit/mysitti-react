import React, { useEffect, useState } from 'react'
import AdverSec from '../../../restaurent/adverSec/AdverSec'
import AppPromo from '../../../restaurent/appPromo/AppPromo'
import BannerSec from '../../../restaurent/bannerSec/BannerSec'
import Slider from "react-slick";
import { UsePlaceAuth } from '../../../../../AppContext/AppContext';
import { apiData } from '../../../../../actions';

const Brewery = () => {
	const [deal, setDeal] = useState([])
	const [groupon, setGroupon] = useState([])
	const [loader, setLoader] = useState(false)
	let {place, setPlace} =UsePlaceAuth()
	  var settings = {
		  infinite: true,
		  speed: 500,
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  arrows:true,
		  responsive: [
			{
			  breakpoint: 991,
			  settings: {
				slidesToShow: 3,
			  }
			},
			{
			  breakpoint: 767,
			  settings: {
				slidesToShow: 2,
			  }
			},
			{
			  breakpoint: 499,
			  settings: {
				slidesToShow: 1,
			  }
			}
		  ]
		};
	  const GetDeals = async () => {
		setLoader(true);
		let payload = {
		  city: place,
		  keyword:"brewery"
		};
		let apiRes = await apiData.getYelpDeals(payload);
		if (apiRes?.status >= 200 && apiRes?.status <= 399) {
		  setDeal(apiRes?.data?.data?.businesses);
		  setLoader(false);
		}
	  };
	  const GetGroupon = async () => {
		setLoader(true);
		let payload = {
		  city: place,
		  keyword:"brewery"
		};
		let apiRes = await apiData.getGroupon(payload);
		if (apiRes?.status >= 200 && apiRes?.status <= 399) {
		  setGroupon(apiRes?.data?.data?.data?.travelExperienceProducts?.resultList);
		  console.log("groupon",apiRes?.data?.data?.data?.travelExperienceProducts?.resultList);
		  setLoader(false);
		}
	  };
	  useEffect(() => {
	  GetDeals();
	  GetGroupon();
	  }, [place])
  return (
    <div>
      <BannerSec/>
      <AdverSec />
      <div className="itemBox">

	  <div class="rind-the-right-section comedy-sec">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="white-box-area inner rest-deals">
							<div class="row">
								<div class="col-lg-12 col-12">
									<section class="client-sec brewery">
										<div class="testimonial-section products">
											<div class="head-yelp">
												<h3>Groupon Performing Brewery</h3>
											</div>
											<div class="owl-carousel owl-theme" id="ProductSlide-audio">
											<Slider {...settings}>
											{
groupon&&groupon?.map((ele)=>
<div data-aos="zoom-in-left" class="testimonial-block product">
  <a href={ele.link}><div class="discount-block">
      <img src={ele.imageLink}/>
      <div class="discount-content">
          <h3>{ele.title.length>=20?`${ele.title.slice(0,20)}...`:ele.title}</h3>
          <div class="stars">
              <ul>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
              </ul>
              <p>(339 Reviews)</p>
          </div>
      </div>
      <div style={{display:"flex",justifyContent:"space-between"}}>
       <div><span style={{textDecoration:"line-through"}}>${ele.price.amount}</span> <span>${ele.salePrice.amount}</span></div>
       <div ><span class="fa fa-map-marker" style={{color:"#FE4D00"}}></span> <span>{ele.locationName}</span></div>
      </div>
  </div></a>
</div>)
                                        }
											</Slider>


											</div>
										</div>

										<div class="testimonial-section products">
											<div class="head-yelp new">
												<h3> Deals on <img src="assets/images/yelpnew.png"/></h3>
											</div>
											<div class="owl-carousel owl-theme" id="ProductSlide-audio2">
												<Slider {...settings}>
												{
  deal&&deal?.map((ele)=>
  <div data-aos="zoom-in-right" class="testimonial-block product">
  <div class="discount-block">
      <a href={ele.url}><img src={ele.image_url}/></a>
      <div class="discount-content">
      <a href={ele.url}><h3>{ele.name}</h3></a>
	  <div class="stars">
																<ul>

																	<li><i class="fa fa-star" aria-hidden="true"></i></li>
																	<li><i class="fa fa-star" aria-hidden="true"></i></li>
																	<li><i class="fa fa-star" aria-hidden="true"></i></li>
																	<li><i class="fa fa-star" aria-hidden="true"></i></li>
																</ul>
																<p>({ele.review_count} reviews)</p>
															</div>
															<p>{ele.location.address1?ele.location.address1:ele.location.display_address[0]}</p>

      </div>
	  <div class="comedy-detail">
															<ul>
																<li><i class="fa fa-map-marker" aria-hidden="true"></i>{ele.location.display_address}</li>
																<li><i class="fa fa-phone" aria-hidden="true"></i>{ele.display_phone}</li>
															</ul>
														</div>
  </div>
</div>
  )
}

												</Slider>

										   </div>
										</div>

									</section>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
      </div>

    <AppPromo />
    </div>
  )
}

export default Brewery