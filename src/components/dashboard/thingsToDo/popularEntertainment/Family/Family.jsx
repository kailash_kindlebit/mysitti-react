import React, { useEffect, useRef, useState } from 'react'
import Header from '../../../../header/Header'
import AdverSec from '../../../restaurent/adverSec/AdverSec'
import BannerSec from '../../../restaurent/bannerSec/BannerSec'
import Slider from "react-slick";
import AppPromo from '../../../restaurent/appPromo/AppPromo';
import { UsePlaceAuth } from '../../../../../AppContext/AppContext';
import { apiData } from '../../../../../actions';
import DataLoader from '../../../../../Utils/DataLoader';
const Family = () => {
	const [deal, setDeal] = useState([])
	const [popularDeal, setPopularDeal] = useState([])
	const [keyword, setKeyword] = useState("")
	console.log("keyword",keyword);
	const [restaurant, setRestaurant] = useState([])
	let   { place } = UsePlaceAuth()
	const [tourData, setTourData] = useState([]);
	const [loader, setLoader] = useState(false);
	const TopDealsRef = useRef();
	var settings = {
		infinite: true,
		speed: 500,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 499,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	};
	var settings1 = {
		infinite: true,
		speed: 500,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 499,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	};
	const GetDeals = async () => {
		setLoader(true);
		let payload = {
			city: place,
			keyword: "attraction"
		};
		let apiRes = await apiData.getYelpDeals(payload);
		if (apiRes?.status >= 200 && apiRes?.status <= 399) {
			setDeal(apiRes?.data?.data?.businesses);
			setLoader(false);
		}
	};
	const GetGroupon = async () => {
		setLoader(true);
		let payload = {
			city: place,
			keyword: "restaurant"
		};
		let apiRes = await apiData.getGroupon(payload);
		if (apiRes?.status >= 200 && apiRes?.status <= 399) {
			setRestaurant(apiRes?.data?.data?.data?.travelExperienceProducts?.resultList);
			console.log("groupon", apiRes?.data?.data?.data?.travelExperienceProducts?.resultList);
			setLoader(false);
		}
	};
	const GetTourTravel = async () => {
		setLoader(true);
		let payload = {
			city: place,
		};
		let apiRes = await apiData.getToursTravel(payload);
		if (apiRes?.status >= 200 && apiRes?.status <= 399) {
			setTourData(apiRes?.data?.tourstravel);
			setLoader(false);
		}
	};
	const handlePopularDealClick = async () => {
		setLoader(true);
		let payload = {
			city: place,
			keyword: keyword
		};
		let apiRes = await apiData.getYelpDeals(payload);
		if (apiRes?.status >= 200 && apiRes?.status <= 399 && payload.keyword!=""||null||undefined) {
			setPopularDeal(apiRes?.data?.data?.businesses);
			console.log("data",apiRes?.data?.data?.businesses)
			setLoader(false);
		}
	}
	const handleScroll=()=>{
		TopDealsRef.current.scrollIntoView({ behavior: "smooth" })
	}
	useEffect(() => {
		GetDeals()
		GetGroupon()
		GetTourTravel()
		handlePopularDealClick()
	}, [place,keyword])
	return (
		<div>
			<BannerSec />
			<AdverSec />
			<div class="rind-the-right-section comedy-sec">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="white-box-area inner rest-deals">
								<div class="row">
									<div class="col-lg-12 col-12">
										<section class="client-sec comedy">
											<div class="testimonial-section products">
												<div class="head-yelp">
													<h3>Popular things to do</h3>
													<p>Find fun places to see and things to do experience the art, museums, music, zoo</p>
												</div>
												<div class="owl-carousel owl-theme" id="ProductSlide-audio">
													<Slider {...settings}>
														<div class="testimonial-block product">
															<div class="discount-block"onClick={()=>{handlePopularDealClick();handleScroll()}}>
																<img src="assets/images/th1.png" />
																<div class="discount-content things">
																	<h3 onClick={()=>setKeyword("Museum")}>Museum</h3>
																	<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
																</div>
															</div>
														</div>
														<div class="testimonial-block product">
															<div class="discount-block" onClick={()=>{handlePopularDealClick();handleScroll()}}>
																<img src="assets/images/th2.png" />
																<div class="discount-content things">
																	<h3 onClick={()=>setKeyword("Sightseeing")}>Sightseeing/tours</h3>
																	<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
																</div>
															</div>
														</div>
														<div class="testimonial-block product">
															<div class="discount-block" onClick={()=>{handlePopularDealClick();handleScroll()}}>
																<img src="assets/images/th3.png" />
																<div class="discount-content things">
																	<h3 onClick={()=>setKeyword("Day trip")}>Day Trip</h3>
																	<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
																</div>
															</div>
														</div>
														<div class="testimonial-block product">
															<div class="discount-block" onClick={()=>{handlePopularDealClick();handleScroll()}}>
																<img src="assets/images/th4.png" />
																<div class="discount-content things">
																	<h3 onClick={()=>setKeyword("Top attractions")}>Top Attractions</h3>
																	<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
																</div>
															</div>
														</div>
														<div class="testimonial-block product">
															<div class="discount-block"onClick={()=>{handlePopularDealClick();handleScroll()}}>
																<img src="assets/images/th1.png" />
																<div class="discount-content things">
																	<h3 onClick={()=>setKeyword("NightLife")}>Night Life</h3>
																	<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
																</div>
															</div>
														</div>
														<div class="testimonial-block product">
															<div class="discount-block"onClick={()=>{handlePopularDealClick();handleScroll()}}>
																<img src="assets/images/th1.png" />
																<div class="discount-content things">
																	<h3 onClick={()=>setKeyword("Shopping")}>Shopping</h3>
																	<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
																</div>
															</div>
														</div>
														<div class="testimonial-block product">
															<div class="discount-block" onClick={()=>{handlePopularDealClick();handleScroll()}}>
																<img src="assets/images/th1.png" />
																<div class="discount-content things">
																	<h3 onClick={()=>setKeyword("Fine dinning")}>Fine Dining</h3>
																	<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
																</div>
															</div>
														</div>
													</Slider>
												</div>
											</div>

											<div class="testimonial-section products family-sec">
												<div class="head-yelp">
													<h3>Where to stay</h3>
													<p>Low hotel rates for luxury, comfort, pet-friendly rooms</p>
												</div>
												<div class="slider-section discount-section stay-sec">
													<div class="container">
														<div class="row">
															<div class="col-12 col-sm-6 col-md-4 col-lg-4 mb-4 p-0">
																<div class="discount-block">
																	<img src="assets/images/th1.png" />
																	<div class="rating">
																		<div class="stars">
																			<div class="reviews-no">
																				<p>8.0</p>
																			</div>
																			<ul>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																			</ul>
																			<p class="riview-last"><img src="assets/images/searc.png" /> 0.29 mi from city center <img src="assets/images/wifi.png" /></p>
																		</div>

																	</div>
																	<div class="discount-content">
																		<h3>New York Marriott Marquis</h3>
																	</div>
																	<div class="discount-action hotels purple-bg city-price">
																		<div class="action-content">
																			<p><span>Price for 1 night</span></p>
																			<p><span class="old-price">$607</span> <span class="new-price">$467</span> <span class="dis-percent">-23%</span></p>
																		</div>
																		<a class="hotel-book" href="#">Book Now</a>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-6 col-md-4 col-lg-4">
																<div class="discount-block">
																	<img src="assets/images/th2.png" />
																	<div class="rating">
																		<div class="stars">
																			<div class="reviews-no">
																				<p>8.6</p>
																			</div>
																			<ul>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																				<p class="riview-last"><img src="assets/images/searc.png" /> 0.43 mi from city center <img src="assets/images/wifi.png" /></p>
																			</ul>
																		</div>
																	</div>
																	<div class="discount-content">
																		<h3>Hampton Inn Times Square C</h3>
																	</div>
																	<div class="discount-action hotels saff-bg city-price">
																		<div class="action-content">
																			<p><span>Price for 1 night</span></p>
																			<p><span class="old-price">$607</span> <span class="new-price">$467</span> <span class="dis-percent">-23%</span></p>
																		</div>
																		<a class="hotel-book" href="#">Book Now</a>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-6 col-md-4 col-lg-4 p-0">
																<div class="discount-block city">
																	<img src="assets/images/th3.png" />
																	<div class="rating">
																		<div class="stars">
																			<div class="reviews-no">
																				<p>8.0</p>
																			</div>
																			<ul>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																				<li><i class="fa fa-star" aria-hidden="true"></i></li>
																			</ul>
																			<p class="riview-last"><img src="assets/images/searc.png" /> mi from city center <img src="assets/images/wifi.png" /></p>
																		</div>
																	</div>
																	<div class="discount-content">
																		<h3>citizenM New York Times Square</h3>
																	</div>
																	<div class="discount-action hotels purple-bg city-price">
																		<div class="action-content">
																			<p><span>Price for 1 night</span></p>
																			<p><span class="old-price">$607</span> <span class="new-price">$467</span> <span class="dis-percent">-23%</span></p>
																		</div>
																		<a class="hotel-book" href="#">Book Now</a>
																	</div>

																</div>
															</div>
															<div class="view-tag">
																<a href="#">View All</a>
															</div>
															<div class="powerdby">
																<a href="#">Hotel rating provided by TrustYou™ <img src="assets/images/pwrd.png" /></a>
																<p>Powered by <b>Travelpayouts</b></p>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="testimonial-section products family-sec">
												<div class="head-yelp">
													<h3>Top Attractions in {place}</h3>
												</div>
												{

												}
												<div class="slider-section discount-section stay-sec">
													<div class="container">
														<div class="row">
															{
																deal && deal?.map((ele) =>
																	<div class="col-lg-4 col-12 p-0">
																		<a href={ele.url}>

																			<div class="discount-block blog">
																				<img src={ele.image_url} />
																				<p class="trav-sec desti"> <span>{ele.location.city}</span></p>
																				<div class="time-formate">
																					<p><img src="assets/images/lcation.png" />{ele.location.address1 ? ele.location.address1 : ele.location.display_address[0]}</p>
																				</div>
																				<h3>{ele.name}</h3>
																			</div>
																		</a>
																	</div>
																)
															}
														</div>
													</div>
												</div>
											</div>

											<div class="testimonial-section products">
												<div class="head-yelp">
													<h3>Tours & Travel</h3>
													<p>Enjoy the scenic views of National Parks</p>
												</div>
												<div class="owl-carousel owl-theme" id="ProductSlide-audio3">
													<Slider {...settings}>
														{
															tourData && tourData?.map((ele) =>
																<div data-aos="zoom-in-right" className="testimonial-block product">
																	<div className="discount-block">
																		<a href={ele.link}><img src={ele.image_link} alt="" /></a>
																		<div className="discount-content things">
																			<a href={ele.link}><h3>{ele.title}</h3></a>
																		</div>
																	</div>
																</div>
															)
														}

													</Slider>
												</div>
											</div>

											<div class="testimonial-section products family-sec">
												<div class="head-yelp">
													<h3>Restraurant Deals</h3>
													<p>Save Yourself Or Family Money With Meal Deals</p>
												</div>
												<div class="slider-section discount-section stay-sec">
													<div class="container">
														<div class="row">
															<Slider {...settings1}>
																{
																	restaurant && restaurant?.map((ele) =>
																		<div class="col-12 col-sm-6 col-md-4 col-lg-4 mb-4 p-0">
																			<div class="discount-block family">
																				<img src={ele.imageLink} />

																				<div class="discount-content">
																					<h3>{ele.title}</h3>
																					<p>{ele.description.length>50?`${ele.description.slice(0,50)}...`:ele.description}</p>
																				</div>
																				<div class="discount-action hotels purple-bg city-price">
																					<div class="action-content">
																						<p><span>{ele.locationName}</span></p>
																						<p><span class="old-price">${ele.price.amount}</span> <span class="new-price">${ele.salePrice.amount}</span> <span class="dis-percent">- {Math.round((Number(ele.price.amount) - Number(ele.salePrice.amount)) * 100 / Number(ele.price.amount))}%</span></p>
																					</div>
																					<a class="hotel-book" href={ele.link}>Book Now</a>
																				</div>
																			</div>
																		</div>
																	)
																}
															</Slider>
														</div>
													</div>
												</div>
											</div>
											<div class="testimonial-section products" ref={TopDealsRef}>
												<div class="head-yelp">
												{
													keyword===""?"":<h3>{keyword} in {place}</h3>
												}
												</div>
												{
													!loader ?												<div class="row" >
													{
														popularDeal && popularDeal?.map((ele) =>

															<div class="col-12 col-sm-6 col-md-3 col-lg-3">
																{console.log("item",ele.url)}
																<a href={ele.url}><div class="discount-block dig">
																	<img src={ele.image_url} />
																	<div class="discount-content">
																		<h3>{ele.name}</h3>
																	</div>

																	<div class="stars">
																		<ul>
																			<li><i class="fa fa-star" aria-hidden="true"></i></li>
																			<li><i class="fa fa-star" aria-hidden="true"></i></li>
																			<li><i class="fa fa-star" aria-hidden="true"></i></li>
																			<li><i class="fa fa-star" aria-hidden="true"></i></li>
																		</ul>
																		<p>{ele.review_count} Reviews</p>
																	</div>
																	<div class="sticker"><img src="assets/images/dig.png" /></div>
																</div></a>
															</div>
														)
													}
												</div>
												:<DataLoader/>
												}

											</div>
										</section>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>

			<AppPromo />
		</div>
	)
}

export default Family