//To show comedy section in things to do
import React, { Fragment, useState, useEffect } from "react";
import Slider from "react-slick";
import { apiData } from "../../../../actions";
import { Rate } from "antd";
import { UsePlaceAuth } from "../../../../AppContext/AppContext";
import DataLoader from "../../../../Utils/DataLoader";

function Comedy() {
	 let {place} =UsePlaceAuth()
   const [loader, setLoader] = useState(false);
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  const [getComedyData, setComedyData] = useState([]);
  const GetComedy = async () => {
    setLoader(true);
    let payload = {
      city: place,
    };
    let apiRes = await apiData.getComedy(payload);
    setComedyData(apiRes?.data?.comedy.businesses)
    console.log("getcomedy section", apiRes?.data?.comedy.businesses);
    setLoader(false);
  };
  useEffect(() => {
    GetComedy();
  }, [place]);
  return (
    <Fragment>
      {!loader?
          <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>Comedy</h3>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio4">
                    <Slider {...settings}>
                      {getComedyData.map((ele)=>
                                  <div data-aos="zoom-in-right" className="testimonial-block product">
                                  <div className="discount-block">
                                    <img src={ele.image_url} alt="" />
                                    <div className="discount-content things">
                                      <h3>{ele.name}</h3>
                                      <p>{ele.location.display_address[0]}</p>
                                      <p>{ele.location.display_address[1]}</p>
                                      <p>{ele.location.phone}</p>
                                      <a href={ele.url}>
                                        <i className="fa fa-star-o" aria-hidden="true"></i>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                )}
          </Slider>
        </div>
      </div>
    :""
    }

    </Fragment>
  );
}

export default Comedy;
