import React, { Fragment, useState, useEffect } from "react";

import Slider from "react-slick";

import { apiData } from "../../../../actions";
import { UsePlaceAuth } from "../../../../AppContext/AppContext";
import DataLoader from "../../../../Utils/DataLoader";

function Winery() {
	 let {place} =UsePlaceAuth()
   const [loader, setLoader] = useState(false);

  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  const [wineryData, setWineryData] = useState([]);

  const GetWinery = async () => {
    setLoader(true);
    let payload = {
      city: place,
    };

    let apiRes = await apiData.getWinery(payload);
    setWineryData(apiRes?.data?.winery?.businesses);
    setLoader(false);
    console.log("winery data",apiRes?.data?.winery?.businesses)
  };
  useEffect(() => {
    GetWinery();
  }, [place]);

  return (
    <Fragment>
      {
        !loader?
        <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>Winery</h3>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio2">
        <Slider {...settings}>
          {
           wineryData.map((ele)=>
           <div data-aos="zoom-in-right" className="testimonial-block product">
           <div className="discount-block">
             <a href={ele.url}><img src={ele.image_url} alt="" /></a>
             <div className="discount-content things">
             <a href={ele.url}><h3>{ele.name}</h3></a>
               <a href="#/">
                 <i className="fa fa-star-o" aria-hidden="true"></i>
               </a>
             </div>
           </div>
         </div>
           )
          }

          </Slider>
        </div>
      </div>:
      <DataLoader/>
      }

    </Fragment>
  );
}

export default Winery;
