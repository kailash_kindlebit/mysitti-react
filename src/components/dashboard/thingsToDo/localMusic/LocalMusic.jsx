import React, { Fragment } from "react";
import { Link } from "react-router-dom";

import Slider from "react-slick";

function LocalMusic() {
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  return (
    <Fragment>
      <div className="testimonial-section products">
        <div className="head-yelp">
          <h3>Local Music</h3>
          <p>Enjoy the music of New York local artist</p>
        </div>
        <div className="owl-carousel owl-theme" id="ProductSlide-audio2">
          <Slider {...settings}>
            <div data-aos="zoom-in-right" className="testimonial-block product">
              <div className="discount-block">
              <Link to="/rock"><img src="assets/images/th5.png" alt="" /></Link>
                <div className="discount-content things">
                  <Link to="/rock"><h3>Rock</h3></Link>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-left" className="testimonial-block product">
              <div className="discount-block">
              <Link to="/blues"><img src="assets/images/th6.png" alt="" /></Link>
                <div className="discount-content things">
                <Link to="/blues"><h3>Blues</h3></Link>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-right" className="testimonial-block product">
              <div className="discount-block">
              <Link to="/country"><img src="assets/images/th7.png" alt="" /></Link>
                <div className="discount-content things">
                <Link to="/country"><h3>Country</h3></Link>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-left" className="testimonial-block product">
              <div className="discount-block">
              <Link to="/jazz"><img src="assets/images/th8.png" alt="" /></Link>
                <div className="discount-content things">
                <Link to="/jazz"><h3>Jazz</h3></Link>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
            <div data-aos="zoom-in-left" className="testimonial-block product">
              <div className="discount-block">
              <Link to="/concerts"><img src="assets/images/th15.png" alt="" /></Link>
                <div className="discount-content things">
                <Link to="/concerts"><h3>Concerts</h3></Link>
                  <a href="#/">
                    <i className="fa fa-star-o" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
          </Slider>
        </div>
      </div>
    </Fragment>
  );
}

export default LocalMusic;
