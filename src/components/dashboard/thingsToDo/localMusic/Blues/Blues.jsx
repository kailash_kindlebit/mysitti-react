import axios from 'axios'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import Slider from 'react-slick'
import { apiData } from '../../../../../actions'
import { UsePlaceAuth } from '../../../../../AppContext/AppContext'
import DataLoader from '../../../../../Utils/DataLoader'
import AdverSec from '../../../restaurent/adverSec/AdverSec'
import AppPromo from '../../../restaurent/appPromo/AppPromo'
import BannerSec from '../../../restaurent/bannerSec/BannerSec'
import MusicMenu from '../MusicMenuData'

const Blues = () => {
  const [deal, setDeal] = useState([])
  const [musicType, setMusicType] = useState("blues")
  const [musicData, setMusicData] = useState([])
  const [groupon, setGroupon] = useState([])
  const [loader, setLoader] = useState(false)
  let { place, setPlace } = UsePlaceAuth()
  const DataJson = MusicMenu ?? [];
  const [testing, setTesting] = useState(["Blues"]);
  const [scrollComponent, setScrollComponent] = useState(false);
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 499,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  const GetDeals = async () => {
    setLoader(true);
    let payload = {
      city: place,
      keyword:"blues"
    };
    let apiRes = await apiData.getYelpDeals(payload);
    if (apiRes?.status >= 200 && apiRes?.status <= 399) {
      setDeal(apiRes?.data?.data?.businesses);
      setLoader(false);
    }
  };
  const GetGroupon = async () => {
    setLoader(true);
    let payload = {
      city: place,
      keyword:"blues"
    };
    let apiRes = await apiData.getGroupon(payload);
    if (apiRes?.status >= 200 && apiRes?.status <= 399) {
      setGroupon(apiRes?.data?.data?.data?.travelExperienceProducts?.resultList);
      console.log("groupon", apiRes?.data?.data?.data?.travelExperienceProducts?.resultList);
      setLoader(false);
    }
  };
  const GetMusic = () => {
    setLoader(true);
    axios
      .get(
        `${process.env.REACT_APP_BASE_URL}category-filter?city=${place}&keyword=${musicType}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then(
        function (response) {

          setMusicData(response?.data?.data?._embedded?.events)
          setLoader(false);
          //   console.log("musicData",response?.data?.data?._embedded?.events)
        },
        (error) => {
          console.log(error);
        }
      );
  }
  useEffect(() => {
    GetDeals();
    GetGroupon();
    GetMusic()
  }, [place, musicType])
  const handleDeliveryType = (e) => {
    const { checked } = e.target;
    setScrollComponent(checked);

    if (checked) {
      const element = document.getElementById("family_ticketMaster");
      element.scrollIntoView({ behavior: "smooth" });
    }
    const getValue = e.target.value;
    console.log("getvalue",getValue);
    setMusicType(getValue);
    setTesting((prevState) => {
      return checked
        ? [prevState, getValue]
        : prevState?.filter((item) => item !== getValue);
    });
  };
  return (
    <div>
      <BannerSec />
      <AdverSec />
      <div className="itemBox">
        <div class="rind-the-right-section comedy-sec">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                <div class="sidebar-listing">
                  <div class="specialities-checkbox">
                    <div class="searcher-sec">
                      <label class="custom-control-label">Search By Name</label>
                      <div class="form-group">
                        <input type="Name" class="form-control" id="exampleFormControlInput1" placeholder={place} />
                      </div>
                    </div>
                  </div>
                  <div class="specialities-checkbox">
                    <div class="listing-check">
                      <div className='listing-check'>
                        <h4>CATEGORY</h4>
                        {
                          DataJson.map((ele) =>
                            <>

                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input"
                                  id={ele?.label}
                                  value={ele?.label}
                                  checked={testing.includes(ele?.label)}
                                  onChange={(e) => {
                                    handleDeliveryType(e);
                                    GetMusic();
                                  }}
                                />
                                <label class="custom-control-label" htmlFor={ele.label}>{ele.label}</label>
                              </div>
                            </>


                          )
                        }
                      </div>
                      <div class="listing-check">
                        <img src='assets/images/ad1-performing arts.gif' style={{ width: "100%" }} />
                      </div>
                      <div class="listing-check">
                        <img src='assets/images/ad2-performing arts.gif' style={{ width: "100%" }} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                <div class="white-box-area inner rest-deals">
                  <div class="row">
                    <div class="col-md-12 col-12">
                      <div class="heading-content">
                        <div class="content-sec">
                          <p>Showing 3 of 257 places</p>
                          <div class="custom-select-box">
                            <span>Sort by </span>
                            <select class="form-select" aria-label="Default select example">
                              <option selected="">Recommended</option>
                              <option value="1">One</option>
                              <option value="2">Two</option>
                              <option value="3">Three</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12 col-12">
                      <section class="client-sec comedy">
                        <div class="testimonial-section products">
                          <div class="head-yelp">
                            <h3>Groupon Performing Blue Music</h3>
                          </div>
                          <div class="owl-carousel owl-theme" id="ProductSlide-audio">
                            <Slider {...settings}>
                              {
                                groupon && groupon?.map((ele) =>
                                  <div data-aos="zoom-in-left" class="testimonial-block product">
                                    <a href={ele.link}><div class="discount-block">
                                      <img src={ele.imageLink} />
                                      <div class="discount-content">
                                        <h3>{ele.title.length >= 20 ? `${ele.title.slice(0, 20)}...` : ele.title}</h3>
                                        <div class="stars">
                                          <ul>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                          </ul>
                                          <p>(339 Reviews)</p>
                                        </div>
                                      </div>
                                      <div style={{ display: "flex", justifyContent: "space-between" }}>
                                        <div><span style={{ textDecoration: "line-through" }}>${ele.price.amount}</span> <span>${ele.salePrice.amount}</span></div>
                                        <div ><span class="fa fa-map-marker" style={{ color: "#FE4D00" }}></span> <span>{ele.locationName}</span></div>
                                      </div>
                                    </div></a>
                                  </div>
                                )
                              }
                            </Slider>
                          </div>
                        </div>
                        <div class="testimonial-section products">
                          <div class="head-yelp new">
                            <h3> Deals on <img src="assets/images/yelpnew.png" /></h3>
                          </div>
                          <div class="owl-carousel owl-theme" id="ProductSlide-audio2">
                            <Slider {...settings}>
                            {
  deal&&deal?.map((ele)=>
  <div data-aos="zoom-in-right" class="testimonial-block product">
  <div class="discount-block">
      <a href={ele.url}><img src={ele.image_url}/></a>
      <div class="discount-content">
      <a href={ele.url}><h3>{ele.name}</h3></a>
	  <div class="stars">
																<ul>

																	<li><i class="fa fa-star" aria-hidden="true"></i></li>
																	<li><i class="fa fa-star" aria-hidden="true"></i></li>
																	<li><i class="fa fa-star" aria-hidden="true"></i></li>
																	<li><i class="fa fa-star" aria-hidden="true"></i></li>
																</ul>
																<p>({ele.review_count} reviews)</p>
															</div>
															<p>{ele.location.address1?ele.location.address1:ele.location.display_address[0]}</p>

      </div>
	  <div class="comedy-detail">
															<ul>
																<li><i class="fa fa-map-marker" aria-hidden="true"></i>{ele.location.display_address}</li>
																<li><i class="fa fa-phone" aria-hidden="true"></i>{ele.display_phone}</li>
															</ul>
														</div>
  </div>
</div>
  )
}

                            </Slider>

                          </div>
                        </div>


                        <div class="head-yelp">
                          <h3>Popular shows in {place}</h3>
                        </div>

                        {
                          !loader ?
                            <div class="comedy-bottom-sec family-ticketMaster ticketMaster_sec" id="family_ticketMaster">

                              {
                                musicData && musicData?.map((ele) =>
                                  <div class="slide">
                                    <div class="discount-block">
                                      <a href={ele._embedded.venues.map((i) => i.url)}>
                                        <div class="cities">
                                          <img src={ele.images[0].url} />
                                        </div>
                                        <div class="discount-content">
                                          <h3>{ele.name}</h3> <span><i class="fa fa-map-marker" aria-hidden="true"></i> {ele._embedded.venues.map((i) => i.name)}</span>
                                        </div>
                                        <div class="comedy-add-details">
                                          <p><i class="fa fa-map-marker" aria-hidden="true"></i>{ele._embedded.venues.map((i) => i.address?.line1)}</p>
                                          <p><i class="fa fa-calendar" aria-hidden="true"></i>{moment(ele?.dates.start.localDate).format(
                                            "MMMM Do YYYY"
                                          )}</p>
                                          <p><i class="fa fa-clock-o" aria-hidden="true"></i> {moment(ele?.dates.start.localDate, "HH:mm:ss").format("hh:mm A")}</p>
                                        </div>
                                      </a>
                                      <div class="discount-action hotels">
                                        <a class="hotel-book" href={ele._embedded.venues.map((i) => i.url)} target="_blank">See Ticket </a>
                                      </div>
                                    </div>
                                  </div>
                                )
                              }
                            </div>
                            : <DataLoader/>
                        }

                      </section>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <AppPromo />
    </div>
  )
}

export default Blues