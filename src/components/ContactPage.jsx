import React from 'react'

const ContactPage = () => {
  return (
    <div>ContactPage
      <div class="flex-container">
        <div>
          <i className="fa fa-star" aria-hidden="true"></i>
          <p><span style={{fontWeight:"bold "}}>Address:</span> <span style={{color:"#FE4D00"}}>198 West 21th Street,</span></p>
          <p>Suite 721 New York NY 10016</p>
        </div>
        <div>
        <i className="fa fa-location" aria-hidden="true"></i>
        <a href="mailto:contact@mysittivacation.com"><p><span style={{fontWeight:"bold "}}>Email</span><span style={{color:"#FE4D00"}}>contact@mysittivacation.com</span></p></a>
        </div>
        <div>
        <i className="fa fa-location" aria-hidden="true"></i>
          <a href="tel:+11234523427"><p><span style={{fontWeight:"bold "}}>Phone</span><span style={{color:"#FE4D00"}}>+1 123-452-3427</span></p></a>
        </div>
        <i className="fa fa-location" aria-hidden="true"></i>
      </div>
    </div>
  )
}

export default ContactPage