import React from "react";

function Footer() {
  const handleModeChange=()=>{
    document.body.classList.toggle("dark-mode")
  }
  return (
    <React.Fragment>
      <footer className="footer-section">
        <div className="container">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 mb-4">
              <div className="footer-logo">
                <a className="navbar-brand" href="#/" >
                  <img src="assets/images/logo.svg" alt="" />
                </a>
                <p>
                  We can help you plan the perfect vacation. Our travel website
                  makes it easy to find the ideal trip and book one today! Check
                  out our deals on flights, hotels, cruises, adventure tours,
                  car rentals, tours, and more. We have partnered with more than
                  700+ airlines, over 500,000+ hotel locations, and thousands of
                  travel sites worldwide. With so much to see and do, you want
                  to ensure you've got the best travel plans. That's why we
                  created our site: to help you find a great vacation package
                  you can't find anywhere else.
                </p>
                <ul></ul>
              </div>
              <div className="footer-links">
                <ul>
                  <li>
                    <a href="#/">About Us</a>
                  </li>
                  <li>
                    <a href="#/">DMCA Policy</a>
                  </li>
                  <li>
                    <a href="#/">Term & Conditions</a>
                  </li>
                  <li>
                    <a href="#/">Privacy Policy</a>
                  </li>
                  <li>
                    <a href="#/">Other Term & Conditions</a>
                  </li>
                  <li>
                    <a href="#/">Contact Us</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 mb-4">
              <p>© 2023 mysittivacations.com</p>
            </div>

          </div>
        </div>
        <div class="switch__tab">
				<span class="switch__tab-btn active light-mode-title">Light</span>
				<span class="mode__switch" onClick={handleModeChange}><span></span></span>
				<span class="switch__tab-btn dark-mode-title">Dark</span>
			</div>
      </footer>
    </React.Fragment>
  );
}
export default Footer;
