import React,{createContext,useContext,useState} from "react";
export const AppContext=createContext();
export const AppProvider=({children})=>{
    let [place, setPlace] = useState("New york")
    let [active, setActive] = useState("1")
    const [origin, setOrigin] = useState(place)
	const [destination, setDestination] = useState("")
	const [originDate, setOriginDate] = useState("")
	const [destinationDate, setDestinationDate] = useState("")
    return (
        <AppContext.Provider value={{place, setPlace,active, setActive,origin, setOrigin,destination, setDestination,originDate, setOriginDate,destinationDate, setDestinationDate}}>
             {children}
        </AppContext.Provider>
    )
}
export function UsePlaceAuth(){
    return useContext(AppContext)
}