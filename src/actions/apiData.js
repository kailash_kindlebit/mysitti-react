import axios from "axios";
async function restaurantDealsSearch(payload) {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}restaurant-deals-search?city=${payload?.city}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response?.data?.data;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}

async function restaurantDealsFilterSearch(payload) {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}restaurant-deals-filter-search?city=${payload?.city}&keyword=${payload?.keyword}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response?.data?.data;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}


async function blog() {
  let getResponse;
  await axios
    .get(`${process.env.REACT_APP_BASE_URL}get-wp-blogs`, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}

async function audioTour(payload) {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}get-izi-travel-apis?city=${payload?.city}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}

async function getEscapeRoom(payload) {
  let getResponse;
  await axios
    .get(`${process.env.REACT_APP_BASE_URL}get-escape-room/?city=${payload?.city}`, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}

async function getFamilyAttraction(payload) {
  let getResponse;
  await axios

    .get(
      `${process.env.REACT_APP_BASE_URL}get-family-top-attraction/?city=${payload?.city}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}

async function getWinery(payload) {
  let getResponse;
  await axios
    .get(`${process.env.REACT_APP_BASE_URL}get-winery/?city=${payload?.city}`, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}

async function getComedy(payload) {
  let getResponse;
  await axios
    .get(`${process.env.REACT_APP_BASE_URL}get-comedy/?city=${payload.city}`, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}

async function getToursTravel(payload) {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}get-tours-travel/?city=${payload.city}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}
async function getAdrenalineRush(payload) {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}get-adrenaline-rush/?city=${payload.city}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}
async function getDigInto(payload) {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}get-dig-into/?city=${payload.city}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}

async function getWhereToStay(payload) {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}get-where-to-stay/?city=${payload?.city}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        console.log("avijit",response);
        getResponse = response;
      },
      (error) => {
        console.log(error);
        console.log("avijiterror",error);

      }
    );
  return getResponse;
}
async function getYelpDeals(payload) {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}get-yelp-deals?city=${payload?.city}&keyword=${payload?.keyword}&limit=12`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log("err",error);
      }
    );
  return getResponse;
}
async function getGroupon(payload) {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}get-groupon?city=${payload?.city} ${payload?.keyword}&limit=9`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}
async function musicFilterSearch(payload) {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}category-filter?city=${payload?.city}&keyword=${payload?.keyword}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response?.data;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}

async function getRandomDeals() {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}get-random-deals/?flag=All-Inclusive`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}
async function getShadowAudio(payload) {
  let getResponse;
  await axios
    .get(
      `${process.env.REACT_APP_BASE_URL}get-izi-model-data?main_uiid=${payload.audioId}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
    .then(
      function (response) {
        getResponse = response;
      },
      (error) => {
        console.log(error);
      }
    );
  return getResponse;
}

// eslint-disable-next-line
export default {
  restaurantDealsSearch,
  restaurantDealsFilterSearch,
  blog,
  audioTour,
  getEscapeRoom,
  getFamilyAttraction,
  getWinery,
  getComedy,
  getToursTravel,
  getAdrenalineRush,
  getDigInto,
  getWhereToStay,
  getYelpDeals,
  getGroupon,
  musicFilterSearch,
  getRandomDeals,
  getShadowAudio
};
