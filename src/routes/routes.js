import {BrowserRouter, Route, Routes } from "react-router-dom";

import Layout from "../components/layout/Layout";
import Home from "../components/dashboard/home/Home";
import Blog from "../components/dashboard/blog/Blog";
import Deals from "../components/dashboard/deals/Deals";
import AudioTour from "../components/dashboard/audioTour/AudioTour";
import Restaurent from "../components/dashboard/restaurent/Restaurent";
import ThingsToDo from "../components/dashboard/thingsToDo/ThingsToDo";
import Trip from "../components/dashboard/Trip/Trip";
import ErrorPage from "../components/ErrorPage";
import ContactPage from "../components/ContactPage";
import Rock from "../components/dashboard/thingsToDo/localMusic/Rock/Rock";
import Blues from "../components/dashboard/thingsToDo/localMusic/Blues/Blues";
import Jazz from "../components/dashboard/thingsToDo/localMusic/Jazz/Jazz";
import Country from "../components/dashboard/thingsToDo/localMusic/Country/Country";
import Concerts from "../components/dashboard/thingsToDo/localMusic/Concerts/Concerts";
import PerformingArts from "../components/dashboard/thingsToDo/popularEntertainment/Performing Arts/Performing Arts";
import Brewery from "../components/dashboard/thingsToDo/popularEntertainment/Brewery/Brewery";
import Family from "../components/dashboard/thingsToDo/popularEntertainment/Family/Family";
import ComedyEntertainment from "../components/dashboard/thingsToDo/popularEntertainment/Comedy/ComedyEntertainment";
import CarRental from "../components/dashboard/CarRental/CarRental";
import Flight from "../components/dashboard/Flight/Flight";
import Hotels from "../components/dashboard/restaurent/bannerSec/hotels/Hotels";

const CreateRoutes = () =>
{
    return (
        <>
            <BrowserRouter>
                <Routes>
                <Route path="/" element={<Home />} />
                    <Route element={<Layout />}>
                        <Route path="restaurant" element={<Restaurent />} />
                        <Route path="things-to-do" element={<ThingsToDo />} />
                        <Route path="blog" element={<Blog />} />
                        <Route path="audio-tour" element={<AudioTour />} />
                        <Route path="deals" element={<Deals />} />
                        <Route path="trip" element={<Trip />} />
                        <Route path="contact" element={<ContactPage />} />
                        <Route path="rock" element={<Rock />}/>
                        <Route path="blues" element={<Blues />}/>
                        <Route path="jazz" element={<Jazz />}/>
                        <Route path="country" element={<Country/>}/>
                        <Route path="concerts" element={<Concerts/>}/>
                        <Route path="performing-arts" element={<PerformingArts/>}/>
                        <Route path="brewery" element={<Brewery/>}/>
                        <Route path="comedy" element={<ComedyEntertainment/>}/>
                        <Route path="family" element={<Family/>}/>
                        <Route path="car-rental" element={<CarRental/>}/>
                        <Route path="flight" element={<Flight/>}/>
                        <Route path="hotel" element={<Hotels/>}/>
                    </Route>
                    <Route path="*" element={<ErrorPage/>} />
                </Routes>
            </BrowserRouter>
        </>
    )
}
export default CreateRoutes;
